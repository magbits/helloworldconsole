using System;

/// <summary>
/// Hello World Console Program for PROG10065 - Interactive Application Development
/// The program demonstrates basic input-output for a console application in C# and .NET
/// using implicitly typed variables and interpolated strings.
/// </summary>
class MyFirstCSharpClass
{
   static void Main()
   {
	    Console.WriteLine("Hello .NET World");

        //ask the user for input
        Console.WriteLine("Please fill in the blank in the question below:");
        Console.WriteLine("C# is a(n) ________ language");

        //reads input from the user
        var userAnswer = Console.ReadLine();

        //check the answer and provide the user with feedback
        //no checking for now until we lean if statements
        Console.WriteLine($"C# is a(n) {userAnswer} language");
	
	    //Exercise: print the message "Press any key to exit..."
	    Console.WriteLine("Press any key to exit...");
	    Console.ReadKey();
   }		
}